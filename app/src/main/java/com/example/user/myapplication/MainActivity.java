package com.example.user.myapplication;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Button;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        Fragment objFragment = null;

        switch (position) {
            case 0:
                objFragment = new menu1_Fragment();
                break;
            case 1:
                objFragment = new menu2_Fragment();
                break;
            case 2:
                objFragment = new menu3_Fragment();
                break;


        }
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, objFragment)
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final MediaPlayer intromp = MediaPlayer.create(this, R.raw.intro);

        Button intro = (Button) this.findViewById(R.id.intro);
        intro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intromp.isPlaying()) {
                    intromp.seekTo(0);
                } else {
                    intromp.start();
                }
            }
        });

        final MediaPlayer holyshitmp = MediaPlayer.create(this, R.raw.holyshit);

        final Button holyshit = (Button) this.findViewById(R.id.holyshit);
        holyshit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holyshitmp.isPlaying()) {
                    holyshitmp.seekTo(0);
                } else {
                    holyshitmp.start();
                }
            }
        });

        final MediaPlayer limberthanwatermp = MediaPlayer.create(this, R.raw.limberthanwater);

        Button limberthanwater = (Button) this.findViewById(R.id.limberthanwater);
        limberthanwater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limberthanwatermp.isPlaying()) {
                    limberthanwatermp.seekTo(0);
                } else {
                    limberthanwatermp.start();
                }
            }
        });

        final MediaPlayer sandpapernipplwsmp = MediaPlayer.create(this, R.raw.sandpapernipples);

        Button sandpapernipples = (Button) this.findViewById(R.id.sandpapernipples);
        sandpapernipples.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sandpapernipplwsmp.isPlaying()) {
                    sandpapernipplwsmp.seekTo(0);
                } else {
                    sandpapernipplwsmp.start();
                }
            }
        });

        final MediaPlayer raisemyweinermp = MediaPlayer.create(this, R.raw.raisingmyweiner);

        Button raisemyweiner = (Button) this.findViewById(R.id.raisemyweiner);
        raisemyweiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (raisemyweinermp.isPlaying()) {
                    raisemyweinermp.seekTo(0);
                } else {
                    raisemyweinermp.start();
                }
            }
        });

        final MediaPlayer alientacklesatreemp = MediaPlayer.create(this, R.raw.alientacklesatree);

        final Button alientacklesatree = (Button) this.findViewById(R.id.alientacklesatree);
        alientacklesatree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alientacklesatreemp.isPlaying()) {
                    alientacklesatreemp.seekTo(0);
                } else {
                    alientacklesatreemp.start();
                }
            }
        });

        final MediaPlayer nipplerubbermp = MediaPlayer.create(this, R.raw.nipplerubber);

        Button nipplerubber = (Button) this.findViewById(R.id.nipplerubber);
        nipplerubber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nipplerubbermp.isPlaying()) {
                    nipplerubbermp.seekTo(0);
                } else {
                    nipplerubbermp.start();
                }
            }
        });

        final MediaPlayer shittalkmp = MediaPlayer.create(this, R.raw.shittalk);

        Button shittalk = (Button) this.findViewById(R.id.shittalk);
        shittalk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shittalkmp.isPlaying()) {
                    shittalkmp.seekTo(0);
                } else {
                    shittalkmp.start();
                }
            }
        });
    }
}
